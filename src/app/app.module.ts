import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import { NavbarComponent } from './component/navbar/navbar.component';
import { HomeComponent } from './component/restaurant/home/home.component';
import { ProductComponent } from './component/restaurant/product/product.component';
import { LoginComponent } from './component/login/login.component';
import { RestaurantComponent } from './component/restaurant/restaurant/restaurant.component';
import { AfiliacionComponent } from './component/afiliacion/afiliacion.component';
import { TrabajoComponent } from './component/trabajo/trabajo.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ProductComponent,
    LoginComponent,
    RestaurantComponent,
    AfiliacionComponent,
    TrabajoComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
